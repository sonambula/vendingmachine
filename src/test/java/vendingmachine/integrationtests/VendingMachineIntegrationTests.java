package vendingmachine.integrationtests;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.junit.Test;

import vendingmachine.VendingMachine;
import vendingmachine.change.BasicMoneyChanger;
import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;
import vendingmachine.change.RealisticMoneyChanger;
import vendingmachine.change.solvers.IChangeSolver;
import vendingmachine.change.solvers.RecursiveChangeSolver;



public class VendingMachineIntegrationTests{

	
	@Test
	public void VendingChangeWithBasicMoneyChangeBehaviour(){
		IMoneyChanger changer = new BasicMoneyChanger();
		
		VendingMachine vendingMachine = new VendingMachine(changer);
		
		vendingMachine.add(CoinType.TWO_EURO);
		
		String actual = vendingMachine.getDrink("Coke");
		String expected = "Your Coke and your change:zero";
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void VendingChangeWithRealisticMoneyChangeBehaviour(){
		
		HashMap<CoinType, Integer> initialChange = new HashMap<CoinType, Integer>();
		initialChange.put(CoinType.FIFTY_CTS, 5);
		initialChange.put(CoinType.TEN_CTS, 2);
		initialChange.put(CoinType.TWENTY_CTS,4);
		initialChange.put(CoinType.ONE_EURO,1);
		
		IChangeSolver solver = new RecursiveChangeSolver();		
		IMoneyChanger changer = new RealisticMoneyChanger(initialChange, solver);
		
		VendingMachine vendingMachine = new VendingMachine(changer);
		vendingMachine.add(CoinType.TWO_EURO);
		
		String actual = vendingMachine.getDrink("Coke");
		String expected = "Your Coke and your change:{TEN_CTS=1, TWENTY_CTS=1, FIFTY_CTS=1}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void VendingChangeBiggerCoinIsNotAnOption(){
		
		HashMap<CoinType, Integer> initialChange = new HashMap<CoinType, Integer>();
		initialChange.put(CoinType.FIFTY_CTS, 1);
		initialChange.put(CoinType.TWENTY_CTS, 6);
		
		IChangeSolver solver = new RecursiveChangeSolver();		
		IMoneyChanger changer = new RealisticMoneyChanger(initialChange, solver);
		
		VendingMachine vendingMachine = new VendingMachine(changer);
		vendingMachine.add(CoinType.TWO_EURO);
		
		String actual = vendingMachine.getDrink("Coke");
		String expected = "Your Coke and your change:{TWENTY_CTS=4}";
		
		assertEquals(expected, actual);
	}
}
	
