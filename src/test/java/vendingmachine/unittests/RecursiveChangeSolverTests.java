package vendingmachine.unittests;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;
import vendingmachine.change.solvers.RecursiveChangeSolver;

public class RecursiveChangeSolverTests{
	RecursiveChangeSolver sut;
	Map<CoinType, Integer> change;

	
	@Before
	public void setUp()
	{
		change = new HashMap<CoinType, Integer>();
		change.put(CoinType.FIFTY_CTS, 5);
		change.put(CoinType.TEN_CTS, 2);
		change.put(CoinType.TWENTY_CTS,4);
		change.put(CoinType.ONE_EURO,1);
		
		sut = new RecursiveChangeSolver();
	}

	
	@Test
	public void shouldReturnTheExactChangeAndIs60cts() {
		
		Map<CoinType,Integer> expected = new HashMap<IMoneyChanger.CoinType, Integer>();
		
		expected.put(CoinType.TEN_CTS, 1);
		expected.put(CoinType.FIFTY_CTS, 1);
		

		Map<CoinType,Integer> actual = sut.searchChange(60, change);
		
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void shouldReturnTheExactChangeAndIs90cts() {
		
		Map<CoinType,Integer> expected = new HashMap<IMoneyChanger.CoinType, Integer>();
		
		expected.put(CoinType.TWENTY_CTS, 2);
		expected.put(CoinType.FIFTY_CTS, 1);	
		
		Map<CoinType,Integer> actual = sut.searchChange(90, change);
		
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void shouldReturnTheMoneyIfThereIsNoEnoughtChange() {
		
		Map<CoinType,Integer> expected = new HashMap<IMoneyChanger.CoinType, Integer>();
		
		Map<CoinType,Integer> actual = sut.searchChange(5, change);
		
		
		assertEquals(expected, actual);
	}
	
	
}
	
