package vendingmachine;

import java.util.HashMap;
import java.util.Map;

import vendingmachine.change.Change;
import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;

public class VendingMachine {
	
	
	private IMoneyChanger changer;
	private final Map<String,Integer> products;
	
	public VendingMachine(IMoneyChanger changer){
		
		this.changer = changer;
		products = new HashMap<>();
		products.put("Coke", 120);
		products.put("Acuarious", 150);
		products.put("Red bull", 200);
		products.put("Water", 80);
	 
	}


	public String getDrink(String drink) {
		String res="No enought money";
		Integer cost = products.get(drink);
		int money = changer.getMoney();
		if (cost==null)
		{
			res="This drink doesn't exist";
		}
		else if (money>=cost)
		{
			
			Change change = changer.returnChange(money-cost);
			
			String changeStr = "zero";
			if (change.getChange()!=null)
			{
				changeStr = change.getChange().toString();
			}
			if(change.isEnoughChange())
			{
				res = "Your "+drink;
				res = res + " and your change:" + changeStr;
			}
			else
			{
				res = "There is not enought change... There you are your money back: "+changeStr;
			}
		} 
		return res;
	}


	public void add(CoinType coin) {
		changer.add(coin);
	}

}
