package vendingmachine.change;

import java.util.Map;

import vendingmachine.change.solvers.IChangeSolver;

public class RealisticMoneyChanger implements IMoneyChanger {
	private int money;
	private Map<CoinType, Integer> change;
	private IChangeSolver calculator;

	public RealisticMoneyChanger(Map<CoinType, Integer> initialChange, IChangeSolver calculator) {
		this.change = initialChange;
		this.calculator = calculator;
		money = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vendingmachine.IMoneyChanger#add(java.lang.String)
	 */
	@Override
	public void add(CoinType coin) {

		Integer ncoins = change.get(coin);
		if (ncoins != null)
			change.put(coin, ++ncoins);
		else
			change.put(coin, 1);

		money = money + coin.getValue();

	}

	/*
	 * ! Return change depending of the product cost. If the reset button of the
	 * machine is pressed, the productCost should be zero and returns the
	 * introduced money
	 */

	@Override
	public int getMoney() {

		return money;
	}

	@Override
	public Change returnChange(int toChange) {
		Change res = new Change();
		res.setEnoughChange(true);
		Map<CoinType, Integer> changeBag = calculator.searchChange(toChange, change);
		if (changeBag.isEmpty()) {
			changeBag = calculator.searchChange(money, change);
			res.setEnoughChange(false);
			res.setChange(changeBag);
		} else
			res.setChange(changeBag);
		money = 0;
		return res;

	}
}
