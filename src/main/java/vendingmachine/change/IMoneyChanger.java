package vendingmachine.change;

/*
 * This interface define the changer associated to a vending machine
 */
public interface IMoneyChanger {
	
	public enum CoinType{
		FIVE_CTS(5),
		TEN_CTS(10),
		TWENTY_CTS(20),
		FIFTY_CTS(50),
		ONE_EURO(100),
		TWO_EURO(200);
		private final int value;
		CoinType(int v){value=v;}
		public int getValue() { return value; }
		
	}

	/*!
	 * Add a coin into the money changer.
	 * This coin will be added to the total accumulated in the machine.
	 * @param the coin to add.
	 */
	void add(CoinType coin);

	/*!
	 * Show the accumulated money in cents.
	 * @return the total amount of money.
	 */
	int getMoney();

	/*!
	 * Return the change for a specific product cost taking into a count the 
	 * money added to the machine.
	 * @param int with the product cost in cts.
	 * @return the change with the returned money and if was enough change for the product
	 */
	Change returnChange(int productCost);

}