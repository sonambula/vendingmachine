package vendingmachine.change.solvers;

import java.util.Map;

import vendingmachine.change.IMoneyChanger.CoinType;

/*
 * Interface to be implemented with a solver Algorithms to return the change
 */
public interface IChangeSolver {

	/*
	 * Return the necessary change to fulfill the money to be returned.
	 * The available change will be a map with the key as the coin type and
	 * the value the amount of this coins to be used.
	 * @param moneyToReturn The money to be returned
	 * @param map with the change The available change 
	 * @return map with the change to be returned to the client
	 */
	Map<CoinType, Integer> searchChange(int moneyToReturn, Map<CoinType, Integer> change);

}