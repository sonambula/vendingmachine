package vendingmachine.unittests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import vendingmachine.VendingMachine;
import vendingmachine.change.Change;
import vendingmachine.change.IMoneyChanger;



public class VendingMachineTests{
	VendingMachine sut;
	IMoneyChanger changerMock;

	
	@Before
	public void setUp()
	{
		changerMock = mock(IMoneyChanger.class);
		sut = new VendingMachine(changerMock);
	}
	
	@Test
	public void shouldGetACokeHavingEnoughMoney(){
		Change change=new Change();
		change.setEnoughChange(true);
		change.setChange(new HashMap<>());
		when(changerMock.getMoney()).thenReturn(200);
		when(changerMock.returnChange(80)).thenReturn(change);
		
		String actual = sut.getDrink("Coke");
		
		verify(changerMock).getMoney();
		verify(changerMock).returnChange(80);
		assertTrue(actual.contains("Coke"));
	}
	
	@Test
	public void shouldNotGetACokeIfNotHavingEnoughMoney(){
		when(changerMock.getMoney()).thenReturn(100);
		
		String expected = "No enought money";
		String actual = sut.getDrink("Coke");
		
		verify(changerMock).getMoney();
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldFailIfTheDrinkDoesNotExist(){
		when(changerMock.getMoney()).thenReturn(200);
		
		String expected = "This drink doesn't exist"; //TODO: Better and exception here
		String actual = sut.getDrink("Fanta");
		
		assertEquals(expected, actual);
	}
}
	
