package vendingmachine.change.solvers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import vendingmachine.change.IMoneyChanger.CoinType;

public class RecursiveChangeSolver implements IChangeSolver {
	
	/* (non-Javadoc)
	 * @see vendingmachine.IChangeSolver#searchChange(int, java.util.Map)
	 */
	@Override
	public EnumMap<CoinType, Integer> searchChange(int moneyToReturn, Map<CoinType, Integer> change) {
		EnumMap<CoinType, Integer> changeBag = new EnumMap<>(CoinType.class);
		EnumMap<CoinType, Integer> changeaux = new EnumMap<>(CoinType.class);
		changeaux.putAll(change);
		List<CoinType> l = new ArrayList<>(Arrays.asList(CoinType.values()));
		Collections.reverse(l);
		return searchChange(moneyToReturn, moneyToReturn, changeaux, changeBag, l, l);
	}

	private EnumMap<CoinType, Integer> searchChange(int actualMoney, int totalMoney, EnumMap<CoinType, Integer> change,
			EnumMap<CoinType, Integer> changeBag, List<CoinType> types, List<CoinType> typesAux) {
		if (actualMoney == 0)
			return changeBag;
		else {

			for (CoinType type : types) {

				int newProductCost = actualMoney - type.getValue();
				if (change.containsKey(type) && change.get(type) > 0 && newProductCost >= 0) {

					int c = change.get(type) - 1;
					change.put(type, c);
					changeBag.put(type, changeBag.get(type) != null ? changeBag.get(type) + 1 : 1);
					return searchChange(newProductCost, totalMoney, change, changeBag, types, typesAux);

				} else {
					List<CoinType> l = new ArrayList<>(types);
					l.remove(type);
					return searchChange(actualMoney, totalMoney, change, changeBag, l, typesAux);
				}
			}
			List<CoinType> l = new ArrayList<>(typesAux);
			if (!l.isEmpty())
			{
				
				l.remove(0);
				changeBag.clear();
				return searchChange(totalMoney, totalMoney, change, changeBag, l, l);
			}
			else
				return new EnumMap<>(CoinType.class);
		}
	}

}

